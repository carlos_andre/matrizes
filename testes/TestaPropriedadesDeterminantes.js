const matriz = require("../matriz");
const testCase = require("nodeunit").testCase;

module.exports = testCase({
    "Duas Linhas Ou Colunas Iguais Determinante Zero":function(test){
        var MatrizA = new matriz([
            [1,2,3],
            [4,5,6],
            [1,2,3]
        ]);
        var MatrizB = new matriz([
            [1,4,1],
            [2,5,2],
            [3,6,3]
        ]);
        var detA = MatrizA.CalculaDeterminante();
        var detB = MatrizB.CalculaDeterminante();
        test.expect(2);
        test.equal(detA,0,"Determinante com linhas iguais diferem de zero");
        test.equal(detB,0,"Determinante com colunas iguais diferem de zero");
        test.done();
    },
    "Linha Ou Coluna Valem Zero Determinante Zero":function(test){
        var MatrizA = new matriz([
            [1,2,3],
            [4,5,6],
            [0,0,0]
        ]);
        var MatrizB = new matriz([
            [1,4,0],
            [2,5,0],
            [3,6,0]
        ]);
        var detA = MatrizA.CalculaDeterminante();
        var detB = MatrizB.CalculaDeterminante();
        test.expect(2);
        test.equal(detA,0,"Determinante com linhas nulas diferem de zero");
        test.equal(detB,0,"Determinante com colunas nulas diferem de zero");
        test.done();
    },
    "Matriz Com Combinacao Linear Determinante Zero":function(test){
        var MatrizA = new matriz([
            [1,2,3],
            [4,5,6],
            [2,4,6]
        ]);
    
        var detA = MatrizA.CalculaDeterminante();
        test.equal(detA,0,"Matriz com combinação linear não retornou 0");
        test.done();
    },
    "Determinante Proporcional A Linha Ou Coluna":function(test){
        var MatrizA = new matriz([
            [7,4,6],
            [2,6,10],
            [1,2,9]
        ]);//det = 194
        var matrizB = MatrizA.TransformaLinha(1,x=>x*2);
        var matrizC = MatrizA.TransformaColuna(1,x=>x*2);
    
        var detBase = MatrizA.CalculaDeterminante();
        var detDobroDaLinha = matrizB.CalculaDeterminante();
        var detDobroDaColuna = matrizC.CalculaDeterminante();
    
        test.expect(3);
        test.notEqual(detBase,0,"Determinante base não deve ser igual a zero");
        test.equal(detDobroDaLinha,detBase*2,"Proporcionalidade não mantida entre a linha e o determinante");
        test.equal(detDobroDaColuna,detBase*2,"Proporcionalidade não mantida entre a coluna e o determinante");
        test.done();
    },
    "Determinante Proporcional A Matriz":function(test){
        var MatrizA = new matriz([
            [7,4,6],
            [2,6,10],
            [1,2,9]
        ]);//det = 194
        var k = 5;
        var matrizB = MatrizA.TransformaMatriz(x=>x*k);
        
        var detBase = MatrizA.CalculaDeterminante();
        var detMatrizTransformada = matrizB.CalculaDeterminante();
    
        test.expect(2);
        test.notEqual(detBase,0,"Determinante base não deve ser igual a zero");
        test.equal(detMatrizTransformada,detBase*Math.pow(k,MatrizA.nLinha),"Proporcionalidade não mantida entre a matriz e o determinante");
        test.done();
    },
    "Determinante Igual Para Matriz Transposta":function(test){
        var MatrizA = new matriz([
            [7,4,6],
            [2,6,10],
            [1,2,9]
        ]);
        var MatrizB = MatrizA.MatrizTransposta();
    
        var detA = MatrizA.CalculaDeterminante();
        var detB = MatrizB.CalculaDeterminante();
    
        test.strictEqual(detA,detB,"Igualdade entre determinantes não respeitada para matriz transposta");
        test.done();
    },
    "Inverter Linha Ou Coluna Inverte A Determinante":function(test){
        var MatrizA = new matriz([
            [7,4,6],
            [2,6,10],
            [1,2,9]
        ]);
    
        var MatrizB = MatrizA.TrocarLinha(1,2);
        var MatrizC = MatrizA.TrocarColuna(1,2);
        
        var detA = MatrizA.CalculaDeterminante();
        var detB = MatrizB.CalculaDeterminante();
        var detC = MatrizC.CalculaDeterminante();
        test.expect(3);
        test.notEqual(detA,0,"Determinante base não deve ser igual a zero");
        test.equal(detB,-detA,"Inversão de linha não inverteu o sinal do determinante.");
        test.equal(detC,-detA,"Inversão de coluna não inverteu o sinal do determinante.");
        test.done();
    }
});



