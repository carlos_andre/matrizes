var matriz = require("../matriz");
const testCase = require("nodeunit").testCase;
const matrizA = new matriz([
        [1,2,3],
        [4,5,6],
        [7,8,9]
    ]),
    matriz2x2A = new matriz([
        [1,2],
        [3,4]
    ]),
    matriz2x2B = new matriz([
        [1,2],
        [3,4]
    ]),
    matrizI = matriz.MatrizIdentidade(5),
    matrizIdentidade = new matriz([
        [1,0],
        [0,1]
    ]);

module.exports = testCase({
    "Deve verificar igualdade de matrizes":function(test){
        test.ok(matrizI.Equals(matrizI),"Falha ao validar função de igualdade de matrizes");
        test.ok(matriz2x2A.Equals(matriz2x2B),"Matrizes igual não passam no teste Equals");
        test.done();
    },
    "Deve Receber Linha" : function(test){
        test.expect(5);
        for(var i=1;i<=5;i+=1){
            var linha = matrizI.getLinha(i);
            var linhaTest = Array(matrizI.nColuna).fill(0);
            linhaTest[i-1] = 1;
            test.strictEqual(linha.toString(),linhaTest.toString(),"Valor difere do esperado para linha recebida");
        }
        test.done();
    },
    "Deve Receber Coluna": function(test){
        test.expect(5);
        for(var i=1;i<=5;i++){
            var linha = matrizI.getColuna(i);
            var linhaTest = Array(matrizI.nColuna).fill(0);
            linhaTest[i-1] = 1;
            test.strictEqual(linha.toString(),linhaTest.toString(),"Valor difere do esperado para linha recebida");
        }
        test.done();
    },
    "Deve Inverter Linha": function(test){
        var MatrizInversa = new matriz([
            [4,5,6],
            [1,2,3],
            [7,8,9]
        ]);
        test.ok(matrizA.TrocarLinha(1,2).Equals(MatrizInversa),"Linhas não trocadas");
        test.done();
    },
    "Deve Inverter Coluna": function(test){
        var MatrizInversa = new matriz([
            [2,1,3],
            [5,4,6],
            [8,7,9]
        ]);
        test.ok(matrizA.TrocarColuna(1,2).Equals(MatrizInversa),"Colunas não trocadas");
        test.done();
    },
    "Deve Adicionar Linha": function(test){
        var matrizTesteA = matrizA.AdicionarLinha();
        test.expect(2);
        test.equal(matrizTesteA.nLinha,matrizA.nLinha+1);
        test.equal(matrizTesteA.getLinha(matrizTesteA.nLinha).toString(),
            Array(3).fill(0).toString());
        test.done();
    },
    "Deve Adicionar Coluna" : function(test){
        var matrizTesteA = matrizA.AdicionarColuna();
        test.expect(2);
        test.equal(matrizTesteA.nColuna,matrizA.nColuna +1);
        test.equal(matrizTesteA.getColuna(matrizTesteA.nColuna).toString(),
            Array(3).fill(0).toString());
        test.done();
    },
    "Deve Verificar Matriz Identidade" : function(test){
        
        var MatrizIdentidadeGrande = matriz.MatrizIdentidade(50);
        var MatrizNaoIdentidade = new matriz([
            [1,2,3],
            [2,3,4],
            [5,6,7]
        ]);
        test.expect(4);
        test.ok(matrizIdentidade.isIdentidade,"Erro ao verificar a identidade.");
        test.ok(MatrizIdentidadeGrande.isIdentidade,"Erro ao gerar identidade.");
        test.ok(!MatrizNaoIdentidade.isIdentidade,"Falso positivo na checagem da identidade.");
        test.ok(!MatrizIdentidadeGrande.TrocarLinha(2,3).isIdentidade,"Falso positivo na checagem da identidade.");
        test.done();
    },
    "Deve substituir linha e coluna":function(test){
        
        var matrizB = matrizA.SubstituirLinha(1,[0,0,0]);
        var matrizC = matrizA.SubstituirColuna(1,[0,0,0]);
        test.expect(2);
        test.strictEqual(matrizB.getLinha(1).toString(),[0,0,0].toString(),"Linha substituída não corresponde");
        test.strictEqual(matrizC.getColuna(1).toString(),[0,0,0].toString(),"Coluna substituída não corresponde");
        test.done();
    },
    "Multiplica matrizes":function(test){
        var matrizM = matriz.multiplicaMatriz(
            matrizA, matriz.MatrizIdentidade(3)
        );
        test.ok(matrizM.Equals(matrizA));
        test.done();
    },
    "Testa Matriz Inversa":function(test){
        var MatrizInversaA = matriz2x2A.MatrizInversa();
        var matIdentidade = matriz.multiplicaMatriz(matriz2x2A,MatrizInversaA);
        test.ok(matIdentidade.Equals(matriz.MatrizIdentidade(2)),"Multiplicação com matriz inversa não retorna Matriz identidade.");
        test.done();
    }
});
