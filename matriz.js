"use strict";
var i;
var Decimal = require("decimal.js");

var matriz = class {
    constructor(/** @type Array */vetor){
        this.vetor = vetor.map(function(valor){
            var vec=[];
            for(i=0;i<valor.length;i+=1){
                vec.push(Decimal(valor[i]));
            }
            return vec;
        });
    }

    get isMatrizQuadrada(){
        return this.nLinha == this.nColuna;
    }

    get isIdentidade(){
        if(!this.isMatrizQuadrada) return false;
        for(let i = 0; i<this.nLinha;i++)
            for(let j=0;j<this.nColuna; j++){
                if(this.vetor[i][j] != 1 && this.vetor[i][j] != 0)
                    return false;
                if((i==j && this.vetor[i][j].toNumber() != 1)
                            || (i!=j && this.vetor[i][j].toNumber() != 0)){
                    return false;
                }
            }
        return true;

    }

    get nLinha(){
        return this.vetor.length;
    }

    get nColuna(){
        return this.vetor[0].length;
    }

    get(linha,coluna){
        return this.vetor[linha][coluna];
    }

    get cloneVetor(){
        var novaMatriz = [];
        var i=this.vetor.length;
        while(i--) novaMatriz[i] = this.vetor[i].slice();
        return novaMatriz;
    }

    Equals(obj){
        if(this.nColuna !== obj.nColuna || this.nLinha !== obj.nLinha)
            return false;
        
        for (let i = 0; i < this.nLinha; i++) {
            for(let j=0;j< this.nColuna;j++){
                if(this.get(i,j).toNumber() !== obj.get(i,j).toNumber())
                    return false;
            }
        }
        return true;
    }

    CalculaDeterminante() {
        if(this.isMatrizQuadrada){
            if(this.nColuna >2){
                var somaDet = Decimal(0);
                for(var i=1;i<=this.nColuna;i++){
                    somaDet = somaDet.add(this.vetor[0][i-1].mul(this.CalculaCofator(1,i)));
                }
                return somaDet.toNumber();
            }else{
                return this.CalculaDeterminante2x2();
            }
        }else{
            throw "A matriz não é quadrada.";
        }
    }

    CalculaDeterminante2x2(){
        if(this.isMatrizQuadrada && this.nLinha == 2){
            var diagPrincipal  =  this.vetor[0][0].mul(this.vetor[1][1]);
            var diagSec =  this.vetor[0][1].mul(this.vetor[1][0]);
            return diagPrincipal.minus(diagSec).toNumber();
        }else if(this.isMatrizQuadrada && this.nLinha == 1){
            return this.vetor[0][0].toNumber();
        }else{
            throw "A matriz não é quadrada ou o tamanho não é válido.";
        }
    }

    CalculaCofator(linha,coluna){
        if(this.isMatrizQuadrada == false){
            throw "Só é possível calcular o cofator de uma matriz quadrada.";
        }
        var mat = this.RemoverLinha(linha).RemoverColuna(coluna);
        var cofator = mat.CalculaDeterminante() * Math.pow(-1,(linha+coluna));
        return cofator;
    }

    MatrizCofatores(){
        if(this.isMatrizQuadrada == false){
            throw "Só é possível calcular o cofator de uma matriz quadrada.";
        }
        var novaMatriz = [];
        for(var i=1;i<=this.nLinha;i++){
            novaMatriz[i-1] = [];
            for(var j=1;j<=this.nColuna;j++){
                novaMatriz[i-1][j-1] = this.CalculaCofator(i,j);
            }
        }
        return new matriz(novaMatriz);
    }

    MatrizTransposta(){
        var vetor = this.cloneVetor;
        var novaMatriz = [];
        for(var i = 1;i<=this.nLinha;i++){
            novaMatriz[i-1] = [];
            for(var j = 1;j<=this.nLinha;j++){
                novaMatriz[i-1][j-1] = vetor[j-1][i-1];
            }
        }
        return new matriz(novaMatriz);
    }

    MatrizInversa(){
        var det  = this.CalculaDeterminante();
        if(det == 0){
            throw "Não é possível calcular a inversa de matrizes com determinante igual a zero.";
        }
        return this.MatrizCofatores().MatrizTransposta().TransformaMatriz(function (elemento) {
            return elemento.div(det);
        });
    }

    RemoverLinha(linha){
        if((linha)>this.nLinha || (linha-1)<0){
            throw "A linha selecionada é inválida";
        }
        var novaMatriz = [];
        var linhaNova = 0;
        for(var i=0;i<this.nLinha;i++){
            if(i==(linha-1)){
                continue;
            }
            novaMatriz[linhaNova] = this.vetor[i];
            linhaNova++;
        }
        return new matriz(novaMatriz);
    }

    RemoverColuna(coluna){
        if((coluna)>this.nColuna || (coluna-1)<0){
            throw "A coluna selecionada é inválida";
        }
        var novaMatriz=[];
        var linhaNova = 0;
        for(var i=0;i<this.nLinha;i++){
            novaMatriz[i] = [];
            var colunaNova = 0;
            for(var j=0;j<this.nColuna;j++){
                if(j==(coluna-1)){
                    continue;
                }
                novaMatriz[linhaNova][colunaNova] = this.vetor[i][j];
                colunaNova++;
            }
            linhaNova++;
        }
        return new matriz(novaMatriz);
    }

    SubstituirLinha(linha,vetorLinha){
        if((linha)>this.nLinha || (linha-1)<0){
            throw "A linha selecionada é inválida";
        }
        if(vetorLinha.length != this.nColuna){
            throw "O tamanho da linha é inválido";
        }
        var novaMatriz = this.cloneVetor;
        novaMatriz[linha-1] = vetorLinha;

        return new matriz(novaMatriz);
    }

    SubstituirColuna(coluna,vetorColuna){
        if((coluna)>this.nColuna || (coluna-1)<0){
            throw "A coluna selecionada é inválida";
        }
        if(vetorColuna.length != this.nLinha){
            throw "O tamanho da coluna é inválido";
        }
        var novaMatriz = this.cloneVetor;
        for(var i=0;i<this.nLinha;i++)
            novaMatriz[i][coluna-1] = vetorColuna[i];
        
        return new matriz(novaMatriz);
    }

    AdicionarLinha(vetorLinha){
        if(arguments.length == 0 || vetorLinha == null) vetorLinha = [];
        while(vetorLinha.length < this.nColuna){
            vetorLinha.push(0);
        }
        var linha = this.nLinha;
        var novaMatriz = this.cloneVetor;
        novaMatriz[linha] = vetorLinha;
        return new matriz(novaMatriz);
    }

    AdicionarColuna(vetorColuna){
        if(arguments.length == 0 || vetorColuna == null) vetorColuna = [];
        while(vetorColuna.length < this.nColuna){
            vetorColuna.push(0);
        }
        var coluna = this.nColuna;
        var novaMatriz = this.cloneVetor;
        for(var i=0;i<this.nLinha;i++){
            novaMatriz[i][coluna] = vetorColuna[i];
        }
        return new matriz(novaMatriz);
    }

    getLinha(linha){
        return this.cloneVetor[linha-1].map(function(valor)
        {
            return valor.toNumber();
        }
        );
    }

    TrocarLinha(linha1,linha2){
        var linhaTemp1 = this.getLinha(linha1);
        var linhaTemp2 = this.getLinha(linha2);
        return this.SubstituirLinha(linha2,linhaTemp1).SubstituirLinha(linha1,linhaTemp2);
    }

    TrocarColuna(coluna1,coluna2){
        var colunaTemp1 = this.getColuna(coluna1);
        var colunaTemp2 = this.getColuna(coluna2);
        return this.SubstituirColuna(coluna2,colunaTemp1).SubstituirColuna(coluna1,colunaTemp2);
    }

    getColuna(coluna){
        var vColuna = [];
        this.cloneVetor.map(function(valor){
            vColuna.push(valor[coluna-1].toNumber());
        });
        return vColuna;
    }

    TransformaMatriz(fn){
        var novaMatriz = this.cloneVetor;
        for(var i = 0;i<this.nLinha;i++){
            for(var j=0;j<this.nColuna;j++){
                novaMatriz[i][j] = fn(novaMatriz[i][j]);
            }
        }
        return new matriz(novaMatriz);
    }

    TransformaLinha(linha,fn){
        var novaMatriz = this.cloneVetor;
        for(var i=0;i<this.nColuna;i++)
            novaMatriz[linha+1][i] = fn(novaMatriz[linha+1][i]);

        return new matriz(novaMatriz);
    }

    TransformaColuna(coluna,fn){
        var novaMatriz = this.cloneVetor;
        for(var i=0;i<this.nLinha;i++)
            novaMatriz[i][coluna+1] = fn(novaMatriz[i][coluna+1]);

        return new matriz(novaMatriz);
    }

    static MatrizIdentidade(tamanho){
        var novaMatriz = [];
        for(var i=0;i<tamanho;i++){
            novaMatriz[i] = [];
            for(var j=0;j<tamanho;j++){
                if(i == j){
                    novaMatriz[i][j] = 1;
                }else{
                    novaMatriz[i][j] = 0;
                }
            }
        }
        return new matriz(novaMatriz);
    }

    static multiplicaMatriz(MatrizA,MatrizB){
        if(MatrizA.nColuna != MatrizB.nLinha){
            throw "Matrizes inválidas para multiplicação";
        }
        var novaMatriz = [];
        for(var i=0;i<MatrizA.nLinha;i++){
            novaMatriz[i] = [];
            for(var j=0;j<MatrizB.nColuna;j++){
                novaMatriz[i][j] = 0;
                for(var n=0;n<MatrizA.nColuna;n++){
                    novaMatriz[i][j] += (MatrizA.vetor[i][n].mul(MatrizB.vetor[n][j]).toNumber());
                }
            }
        }
        return new matriz(novaMatriz);
    }

    static ResolverSistema(MatrizIncompleta,termosIndependente){
        if(MatrizIncompleta.nColuna > termosIndependente.length){
            throw "Impossível resolver sistemas indeterminados";
        }
        var D = MatrizIncompleta.CalculaDeterminante();
        if(D == 0){
            throw "Impossível resolver sistemas indeterminados.";
        }
        var resultados = [];
        for(var i=0;i<termosIndependente.length;i++){
            resultados[i] = MatrizIncompleta.SubstituirColuna(i+1,termosIndependente).CalculaDeterminante()/D;
        }

        return resultados;
    }

    toString(){
        var ret = "{\n";
        for(var i=0;i<this.nLinha;i++){
            ret+="[";
            ret+= this.vetor[i].join();
            ret+="]\n";
        }
        ret+="}";
        return ret;
    }
};
try {
    module.exports = matriz;
} catch (error) {
    console.log("Não foi possível exportar o módulo.");
}